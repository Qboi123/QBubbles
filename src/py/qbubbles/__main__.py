import os
import pathlib
import re
import sys
import time
from tkinter import Tk, Toplevel, Frame
from typing import Optional, Callable, Union

import qbubbles
from qbubbles.load import Load
from qbubbles.references import References
from qbubbles.registries import Windows, GAME_CONFIG, Scenes, LAUNCHER_CONFIG
# from qbubbles.registry import Registry
from qbubbles.util import NamespacedKey


class FakeWindow(Tk):
    def __init__(self):
        """
        Initialize method of FakeWindow class
        """
        super(FakeWindow, self).__init__()

        # Initialize fake-window
        self.attributes('-alpha', 0.0)
        frame = Frame(self, bg="#373737")
        frame.pack(fill="both", expand=True)
        self.wm_geometry(f"{self.winfo_screenwidth()}x{self.winfo_screenheight()}+0+0")
        # font1 = font.nametofont("TkFixedFont")
        # family = font1.cget("family")
        # # print()
        # Label(frame, bg="#373737", fg="#a7a7a7", text="FakeWindow", font=(family, 20)).pack()
        self.bind("<Map>", self.on_root_deiconify)
        self.bind("<Unmap>", self.on_root_iconify)

        # self.bind("<FocusOut>", self.onRootIconify)
        self.bind("<FocusIn>", self.on_root_deiconify)

        # Fake-window attributes
        self.child: Optional[Toplevel] = None

    # Toplevel follows root taskbar events (minimize, restore)
    # noinspection PyUnusedLocal
    def on_root_iconify(self, evt):
        """
        Iconify event for fake-window

        :param evt:
        :return:
        """
        if self.child is None:
            return
        self.child.withdraw()

    # noinspection PyUnusedLocal
    def on_root_deiconify(self, evt):
        """
        Deiconify event for fake-window

        :param evt:
        :return:
        """

        if self.child is None:
            return
        self.child.wm_attributes("-alpha", 0)
        self.child.deiconify()
        self.child.wm_attributes("-alpha", 1)

    def ready(self):
        self.lower()
        self.iconify()

    def bind_events(self, toplevel):
        """
        Bind events to the child window
        Events:
         :event Destroy: Used for destroying FakeWindow(...) instance when child is destroyed

        :param toplevel:
        :return:
        """

        self.child = toplevel
        self.child.bind(
            "<Destroy>", lambda event: (self.destroy(
            ) if (event.widget == self.child) or (event.widget == self) else None))

        # self.child.bind("<FocusOut>", lambda event: self.onRootIconify(event) if event.widget == \
        #     self.child and self.child.focus_get() else None)
        self.child.bind(
            "<FocusIn>",
            lambda event: (self.on_root_deiconify(
                event
            ) if event.widget == self.child and not self.child.focus_get() else None))


def get_hwnd_dpi(window_handle):
    """
    To detect high DPI displays and avoid need to set Windows compatibility flags

    :param window_handle:
    :return:
    """

    import os
    if os.name == "nt":
        from ctypes import windll, pointer, wintypes
        windll.shcore.SetProcessDpiAwareness(1)
        dpi100pc = 96  # DPI 96 is 100% scaling
        dpi_type = 0  # MDT_EFFECTIVE_DPI = 0, MDT_ANGULAR_DPI = 1, MDT_RAW_DPI = 2
        win_h = wintypes.HWND(window_handle)
        monitor_handle = windll.user32.MonitorFromWindow(win_h, wintypes.DWORD(2))  # MONITOR_DEFAULTTONEAREST = 2
        x = wintypes.UINT()
        y = wintypes.UINT()
        # noinspection PyBroadException
        try:
            windll.shcore.GetDpiForMonitor(monitor_handle, dpi_type, pointer(x), pointer(y))
            return x.value, y.value, (x.value + y.value) / (2 * dpi100pc)
        except Exception:
            return 96, 96, 1  # Assume standard Windows DPI & scaling
    else:
        return None, None, 1  # What to do for other OSs?


def tk_geometry_scale(s, cvt_func):
    """
    Scaled geometry for Tk-window

    @author: Xinzeon
    @param s: The scale
    @param cvt_func:
    @rtype str:
    @return: The tkinter geometry.
    @see Main:
    """

    patt = r"(?P<W>\d+)x(?P<H>\d+)\+(?P<X>\d+)\+(?P<Y>\d+)"  # format "WxH+X+Y"
    r = re.compile(patt).search(s)
    g = str(cvt_func(r.group("W"))) + "x"
    g += str(cvt_func(r.group("H"))) + "+"
    g += str(cvt_func(r.group("X"))) + "+"
    g += str(cvt_func(r.group("Y")))
    return g


def make_tk_dpi_aware(root: Union[Tk, Toplevel]):
    """
    Used for configure a Tk-window to make it DPI-aware

    @param root:
    @return: None
    """
    root.dpiX, root.dpiY, root.dpiScaling = get_hwnd_dpi(root.winfo_id())
    root.tkScale = lambda v: int(float(v) * root.dpiScaling)
    root.tkGeometryScale = lambda s: tk_geometry_scale(s, root.tkScale)


class Main(Toplevel):
    def __init__(self):
        """
        Main-class constructor for Q-Bubbles

        @author Xinzeon
        """

        self.fakeRoot = FakeWindow()

        self.debug = False
        """
        @ivar: The debug flag. Used for debugging.
        @type: C{bool}
        """

        self.pre_run()
        super(Main, self).__init__(self.fakeRoot)
        self.fakeRoot.bind_events(self)
        # self.protocol("WM_DELETE_WINDOW", self.fakeRoot.destroy)

        self.dpiX: float
        self.dpiY: float
        self.dpiScaling: float
        self.tkScale: Callable
        self.tkGeometryScale: Callable

        # noinspection PyTypeChecker
        make_tk_dpi_aware(self)

        Windows.register(self.fakeRoot, NamespacedKey.from_key("fake"))
        Windows.register(self, NamespacedKey.from_key("default"))

        GAME_CONFIG["startTime"] = time.time()

        width = self.winfo_screenwidth()
        height = self.winfo_screenheight()
        self.wm_attributes("-alpha", 0)
        self.geometry(self.tkGeometryScale(f"{width}x{height}+0+0"))
        self.wm_protocol("WM_DELETE_WINDOW", lambda: os.kill(os.getpid(), 0))
        self.update()
        self.deiconify()
        self.overrideredirect(1)
        self.wm_attributes("-alpha", 1)

        # os.chdir(game_dir)

        Scenes.register(Load(Windows.get("qbubbles:default")))

        Load.scenemanager.change_scene("load_screen")

    def _check_arguments(self):
        # Game directory argument.
        if "launcherConfig" not in GAME_CONFIG.keys():
            game_dir: Optional[str] = None
            for argv in sys.argv[1:]:
                if argv.startswith("gameDir="):
                    game_dir = argv[8:]
            if game_dir is None:
                raise RuntimeError("Argument 'gameDir' is not defined, Q-Bubbles cannot continue")
            if not game_dir.endswith("/"):
                game_dir += "/"
            LAUNCHER_CONFIG["gameDir"] = game_dir

        self._fix_workdirectory()

        # Debug argument.
        if "--debug" in sys.argv:
            GAME_CONFIG["launcherConfig"] = qbubbles.gameScene.default_launchercfg
            LAUNCHER_CONFIG["debug"] = True
            self.debug = True

        References()

    @staticmethod
    def _fix_workdirectory():
        try:
            path = str(pathlib.Path(__file__).parent.parent.parent.joinpath("resources"))
            print(path)
            os.chdir(path)  # ...\qbubbles
        except FileNotFoundError:
            os.chdir(os.path.join(LAUNCHER_CONFIG["gameDir"], "data/1.0-alpha3"))

    # noinspection PyProtectedMember
    @staticmethod
    def _fix_meipass():
        """
        Fixes the current directory using sys._MEIPASS
        """

        import sys
        if hasattr(sys, "_MEIPASS"):
            os.chdir(sys._MEIPASS)

    def pre_run(self):
        """
        @author: Xinzeon
        @rtype: None
        @return: None
        """
        # """
        # Pre-run method for some features
        # Features:
        #  - Debug mode uses the --debug commandline argument
        #
        # Returns:
        #
        # """

        self._check_arguments()


if __name__ == '__main__':
    main = Main()
    main.mainloop()
