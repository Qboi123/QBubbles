import io
import json
import os
import tkinter as tk
import warnings
from pathlib import Path
from typing import List, Union, Optional, Dict

import PIL
import yaml
from PIL import ImageTk, Image

from qbubbles.addon import Addon
from qbubbles.lib import utils

from qbubbles.util import NamespacedKey


class Resources(object):
    _assetsPath: Optional[str] = None
    supportedImages = [".png", ".gif"]
    _tkImgs = [".png", ".gif"]
    _files: List[str] = []
    _images: Dict[str, Union[tk.PhotoImage, ImageTk.PhotoImage]] = []

    def __init__(self, path: Union[str, List[str]] = "assets/"):
        Resources._assetsPath = path
        Resources._files = []
        Resources._images = []
        Resources.index()

    @classmethod
    def _recursion_index(cls, path, depth=10):
        items = os.listdir(path)
        ret = []
        for item in items:
            i_abspath = os.path.join(path, item).replace("\\", "/")
            if (os.path.isdir(i_abspath)) and (depth > 0):
                ret.extend(cls._recursion_index(i_abspath, depth - 1))
            elif os.path.isfile(i_abspath):
                ret.append(i_abspath)
            else:
                pass
        return ret

    @classmethod
    def index(cls):
        if type(cls._assetsPath) == str:
            cls._files = cls._recursion_index(cls._assetsPath)
        elif type(cls._assetsPath) == list:
            cls._files = []
            for path in cls._assetsPath:
                cls._files.extend(cls._recursion_index(path))
        for file in cls._files:
            f_ext = os.path.splitext(file)[-1]
            if f_ext in cls.supportedImages:
                if f_ext in cls._tkImgs:
                    cls._images[file] = tk.PhotoImage(file=file)

    @classmethod
    def get_resource(cls, path, mode="r"):
        path = path.replace("\\", "/")
        abspath = os.path.join("assets", path).replace("\\", "/")
        if "w" in mode:
            raise ValueError("write mode is not supported for resources")
        if mode == "r+":
            raise ValueError("write mode is not supported for resources")
        if mode == "r+b":
            raise ValueError("write mode is not supported for resources")
        if abspath in cls._files:
            with open(abspath, mode) as file:
                data = file.read()
            return data
        elif not os.path.exists(abspath):
            raise TypeError("the specified assets path don't exists")
        elif os.path.isdir(abspath):
            raise TypeError("the assets path is a directory, wich is not supported as resource")
        elif os.path.exists(abspath):
            raise TypeError("the specified assets path isn't indexed")
        else:
            raise RuntimeError("an unkown problem occourd when getting the resource")

    @classmethod
    def get_asset(cls, relpath: Path, filename: str, *, addon: Optional[Addon]):
        if addon is None:
            abspath = relpath.absolute() / filename
            strpath = str(abspath)
            with open(strpath, "rb") as file:
                fd = io.BytesIO(file.read())
                im: PIL.Image.Image = PIL.Image.open(fd)
                # fd.close()
            return ImageTk.PhotoImage(im, size=im.size)
        strpath = str(relpath / filename)
        fd = io.BytesIO(addon.zipimport.get_data(strpath).encode())
        im: PIL.Image.Image = PIL.Image.open(fd)
        # fd.close()
        return ImageTk.PhotoImage(im, size=im.size)

    @classmethod
    def get_imagefrom_data(cls, data, filename):
        if filename is not None:
            if filename.lower().endswith(".jpg"):
                dataEnc = io.BytesIO(data)
                img = Image.open(dataEnc)
            elif filename.lower().endswith(".png"):
                dataEnc = io.BytesIO(data)
                img = Image.open(dataEnc)
            else:
                dataEnc = io.BytesIO(data)
                img = Image.open(dataEnc)
        else:
            img = Image.frombytes("RGBA", len(data), data, decoder_name="raw")
        return ImageTk.PhotoImage(img)

    @classmethod
    def get_image(cls, path):
        abspath = os.path.join("assets", path).replace("\\", "/")
        image: Optional[Union[tk.PhotoImage, ImageTk.PhotoImage]] = cls._images.get(abspath, None)

        if image is not None:
            return image
        elif not os.path.exists(abspath):
            raise TypeError("the specified assets path don't exists")
        elif os.path.isdir(abspath):
            raise TypeError("the assets path is a directory, wich is not supported as resource")
        elif os.path.exists(abspath):
            raise TypeError("the specified assets path isn't indexed")
        else:
            raise RuntimeError("an unkown problem occourd when getting the resource")


class TextureConfigLoader(object):
    assetsPath = "assets/"

    # noinspection PySameParameterValue
    def __init__(self, model_type):
        """
        Texture configuration loader, for custom models such as for the bubbles: They have custom values.

        @param model_type: The model type, uses this for the directory where the models are, such as if it is 'bubbles':
         assets/textureconfig/bubbles
        """
        self.modelType = model_type

    @staticmethod
    def generate_bubble_images(min_size, max_size, config):
        images = {}
        # print(min_size, max_size)
        for radius in range(min_size, max_size+1):
            colors = config["Colors"]
            images[radius] = utils.createbubble_image((radius, radius), None, *colors)
        return images

    def load_textureconfigs(self, addon: Optional[Addon]):
        models = {}
        
        path = f"{self.assetsPath}/textureconfig/{self.modelType}"

        if addon is not None:
            self.load_addon_textureconfigs(addon)
        else:
            if not os.path.exists(path):
                raise FileNotFoundError(f"Path '{path}' does not exist")

            for model in os.listdir(path):
                if model.count(".") > 1:
                    raise NameError(f"Model name '{model}' contains multiple dots, but only one is allowed "
                                    f"(for the file-extension)")
                model_path = os.path.join(path, model)
                if model.endswith(".yml"):
                    with open(os.path.join(path, model), 'r') as file:
                        models[NamespacedKey("qbubbles", os.path.splitext(model)[0])] = yaml.safe_load(file.read())
                elif model.endswith(".json"):
                    with open(os.path.join(path, model), 'r') as file:
                        models[NamespacedKey("qbubbles", os.path.splitext(model)[0])] = json.loads(file.read())
                else:
                    if model.count(".") == 0:
                        warnings.warn(
                            f"Skipping model file '{model_path}' because it has no file extension", ResourceWarning)
                        continue
                    warnings.warn(
                        f"Skipping model file '{model_path}' because it has an unknown file extension: "
                        f"{os.path.splitext(model)[1]}", ResourceWarning)
            return models

    def load_addon_textureconfigs(self, addon: Addon):
        from zipimport import zipimporter
        zipimport: zipimporter = addon.get_zipimport()

        models = {}
        path = f"assets/textureconfig/{self.modelType}"
        try:
            data = zipimport.get_data(path)
        except OSError:
            raise FileNotFoundError(f"Path '{path}' does not exist in addon: {addon.get_addonid()}")

        # if not os.path.exists(path):
        #     raise FileNotFoundError(f"Path '{path}' does not exist")

        for model in os.listdir(path):
            if model.count(".") > 1:
                raise NameError(f"Model name '{model}' contains multiple dots, but only one is allowed "
                                f"(for the file-extension)")
            model_path = os.path.join(path, model)
            if model.endswith(".yml"):
                models[NamespacedKey(addon.get_addonid(), os.path.splitext(model)[0])] = yaml.safe_load(data)
            elif model.endswith(".json"):
                models[NamespacedKey(addon.get_addonid(), os.path.splitext(model)[0])] = json.loads(data)
            else:
                if model.count(".") == 0:
                    warnings.warn(
                        f"Skipping model file '{model_path}' because it has no file extension", ResourceWarning)
                    continue
                warnings.warn(
                    f"Skipping model file '{model_path}' because it has an unknown file extension: "
                    f"{os.path.splitext(model)[1]}", ResourceWarning)
        return models

