import json
import os as _os
import string
import sys
import typing as _t
import zipimport
import warnings
from pathlib import Path
from tkinter import PhotoImage
from typing import Type

import PIL as _PIL
import yaml

from qbubbles import bubbleSystem as _bubSystem, components as _components, config as _config, events as _evts, \
    gameScene as _game, gameIO as _gameIO, globals as _g
import qbubbles.init.bubbleInit as _bubblesInit
import qbubbles.init.effectInit as _effectsInit
import qbubbles.init.entityInit as _entitiesInit
import qbubbles.init.gameTypeInit as _mapsInit
import qbubbles.menus.titleMenu as _titleMenu
import qbubbles.scenemanager as _scenemngr
import qbubbles.scenes as _scenes
import qbubbles.utils as _utils
# import qbubbles.registry as _reg
import qbubbles.resources as _res
from qbubbles.effects import BaseEffect
from qbubbles.registries import GAME_CONFIG, Windows, Scenes, LAUNCHER_CONFIG, Addons, GameTypes, Entities, Bubbles, \
    Textures, Effects, TypeRegistry
from qbubbles.util import NamespacedKey, Translator


class Load(_scenemngr.CanvasScene):
    def __init__(self, root):
        super(Load, self).__init__(root)

        self.set_uname("load_screen")

    def __repr__(self):
        return super(_scenemngr.CanvasScene, self).__repr__()

    def pre_initialize(self):
        pass

    def show_scene(self, *args, **kwargs):
        super(Load, self).show_scene(*args, **kwargs)
        self.initialize()

    # noinspection PyPep8Naming
    def initialize(self):
        config_ = _config.Reader(
            "config/startup.dill").get_decoded()

        _gameIO.Logging(_os.path.join(LAUNCHER_CONFIG['gameDir'], "logs"))
        _gameIO.Logging.info("GameLoader", "Logging started.")

        with open("lang/" + config_["Game"]["language"] + ".yaml", "r") as file:
            lang_ = yaml.safe_load(file.read())

        TypeRegistry.register_translator("en", Translator(None, "en"))

        GAME_CONFIG["config"] = config_
        GAME_CONFIG["language"] = lang_

        # Config resolution / positions
        root = Windows.get("default")
        
        # root = root
        GAME_CONFIG["WindowWidth"] = root.tkScale(root.winfo_screenwidth())
        GAME_CONFIG["WindowHeight"] = root.tkScale(root.winfo_screenheight())
        if "--travis" in sys.argv:
            GAME_CONFIG["WindowWidth"] = 1920
            GAME_CONFIG["WindowHeight"] = 1080
        GAME_CONFIG["MiddleX"] = GAME_CONFIG["WindowWidth"] / 2
        GAME_CONFIG["MiddleY"] = GAME_CONFIG["WindowHeight"] / 2

        _gameIO.Logging.info("GameLoader", f"Initialized window geometry: {GAME_CONFIG['WindowWidth']}x{GAME_CONFIG['WindowHeight']}")

        # # Register Xbox-Bindings
        # Registry.register_xboxbinding("A", game.close_present)
        # print("[Game]:", "Starting XboxController")
        # self.xbox = xbox.XboxController()
        # print("[Game]:", "Started XboxController")
        #
        # self.xControl = dict()
        #
        # a = [int(self.xbox.LeftJoystickX * 7), int(self.xbox.LeftJoystickY * 7)]
        # b = [int(self.xbox.RightJoystickX * 7), int(self.xbox.RightJoystickY * 7)]
        # self.xControl["LeftJoystick"] = a
        # self.xControl["RightJoystick"] = b
        # self.xControl["A"] = bool(self.xbox.A)
        # self.xControl["B"] = bool(self.xbox.B)
        # self.xControl["x"] = bool(self.xbox."x")
        # self.xControl[""y""] = bool(self.xbox."y")
        # self.xControl["Start"] = bool(self.xbox.Start)
        # self.xControl["Back"] = bool(self.xbox.Back)
        # self.xControl["LeftBumper"] = bool(self.xbox.LeftBumper)
        # self.xControl["RightBumper"] = bool(self.xbox.RightBumper)
        # self.xControl["LeftTrigger"] = int((self.xbox.LeftBumper + 1) / 2 * 7)
        # self.xControl["RightTrigger"] = int((self.xbox.RightBumper + 1) / 2 * 7)

        title_font = _utils.Font("Helvetica", 50, "bold")
        descr_font = _utils.Font("Helvetica", 15)

        Scenes.register(_scenes.ErrorScene())
        Scenes.register(_scenes.CrashScene())

        sys.excepthook = _scenes.custom_excepthook
        root.report_callback_exception = _scenes.report_callback_exception
        Windows.get("qbubbles:fake").report_callback_exception = _scenes.report_callback_exception

        t0 = self.canvas.create_rectangle(0, 0, GAME_CONFIG["WindowWidth"], GAME_CONFIG["WindowHeight"], fill="#3f3f3f",
                                          outline="#3f3f3f")
        t1 = self.canvas.create_text(GAME_CONFIG["MiddleX"], GAME_CONFIG["MiddleY"] - 2,
                                     text="Loading Mods", anchor="s",
                                     font=title_font.get_tuple(), fill="#afafaf")
        t2 = self.canvas.create_text(GAME_CONFIG["MiddleX"], GAME_CONFIG["MiddleY"] + 2,
                                     text="", anchor="n",
                                     font=descr_font.get_tuple(), fill="#afafaf")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Started on loading game.")

        mods_dir = f"{LAUNCHER_CONFIG['gameDir']}addons/{_g.GAME_VERSION}"

        if not _os.path.exists(mods_dir):
            _os.makedirs(mods_dir)

        _gameIO.Logging.info("ModLoader", f"Searching for addons in {repr(mods_dir)}")

        # mods_path = os.path.abspath(f"{mods_dir}").replace("\\", "/")
        # sys.path.insert(0, mods_path)
        modules = {}
        mainPackageNames = []

        for file in _os.listdir(mods_dir):
            # print(file, os.path.isfile(f"{mods_dir}/{file}"), f"{mods_dir}/{file}")
            # print(file)
            if _os.path.isfile(f"{mods_dir}/{file}"):
                if file.endswith(".pyz"):
                    a = zipimport.zipimporter(f"{mods_dir}/{file}")  # f"{file}.main", globals(), locals(), [])
                    # print(dir(a))
                    try:
                        mainPackage = json.loads(a.get_data("qbubble-addoninfo.json"))["mainPackage"]
                    except OSError:
                        _gameIO.Logging.warning("ModLoader", f"Found non-addon file: {file}")
                        continue
                    if mainPackage in mainPackageNames:
                        raise RuntimeError(f"Illegal package name '{mainPackage}'. "
                                           f"Package name is already in use")

                    if '.' in mainPackage:
                        _gameIO.Logging.error("ModLoader", f"Illegal package name: '{mainPackage}'. "
                                 f"Package name contains a dot")
                        continue
                    error = False
                    for char in mainPackage[1:]:
                        if char not in string.ascii_letters+string.digits:
                            _gameIO.Logging.error("ModLoader", f"Illegal package name: '{mainPackage}'. "
                                     f"Package name contains invalid character '{char}'")
                            error = True
                    if error:
                        continue

                    mainPackageNames.append(mainPackage)

                    module = a.load_module(mainPackage)
                    # _locals = {firstname: module}
                    # _globals = {}
                    # module = eval(f"{mainPackage}", _globals, _locals)
                    # module = module.qplaysoftware.exampleaddon
                    if a.find_module("qbubbles") is not None:
                        raise RuntimeError("Illegal module name: 'qbubbles'")
                    modules[module.ADDONID] = a
                    # print(a)
                else:
                    if file.endswith(".disabled"):
                        _gameIO.Logging.warning("ModLoader", f"Found disabled file: {file}")
                        continue
                    _gameIO.Logging.warning("ModLoader", f"Found non-addon file: {file}")

            # self.canvas.itemconfig(t0, fill="#ff0000")
            # self.canvas.itemconfig(t1, text="Addon loading failed!", fill="#ffa7a7")
            # self.canvas.itemconfig(t2, text=,
            #                        fill="#ffa7a7")
            # self.canvas.create_text(GAME_CONFIG["WindowWidth"]-16, GAME_CONFIG["WindowHeight"]-16,
            #                         text="Press any key or mouse button to continue", anchor="se",
            #                         font=("Helvetica", Registry.get_window("default").tkScale(16), 'bold'), fill="#ffa7a7")
            # Registry.get_window("default").focus_set()
            # self.canvas.bind_all("<Key>", lambda evt: os.kill(os.getpid(), 1))
            # self.canvas.bind_all("<Button>", lambda evt: os.kill(os.getpid(), 1))
            # Registry.get_window("default").protocol("WM_DELETE_WINDOW", lambda: None)
            # self.canvas.mainloop()
        # sys.path.remove(mods_path)

        addon_ids = Addons.keys()

        if len(addon_ids) > 0:
            _gameIO.Logging.info("ModLoader", f"Attempting to load the following addons: {', '.join(list(addon_ids).copy())}")
        else:
            _gameIO.Logging.info("ModLoader", f"No addons found.")

        # print(list(addon_ids))

        addons = []
        for addon_id in list(addon_ids):
            # print(repr(addon_id), type(addon_id))
            if Addons.keys().__contains__(addon_id):
                addon = Addons.get(addon_id)["addon"]
                addon: Type[qbubbles.addon.AddonSkeleton]
                self.canvas.itemconfig(t2, text=addon.name)
                addon.zipimport = None
                if addon.addonID in modules.keys():
                    addon.zipimport = modules[addon.addonID]
                addons.append(addon())

        # print(addons)

        _evts.PreInitializeEvent(self, self.canvas, t1, t2)

        # # Pre-Initialize
        # self.mod_loader.pre_initialize(self)

        # =----- GAME MAPS -----= #
        self.canvas.itemconfig(t1, text="Loading Game Types")
        self.canvas.itemconfig(t2, text="Initialize gametypes")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Loading Game Types...")

        # try:
        game_types = _mapsInit.init_gametypes()
        i = 1
        for game_type in game_types:
            self.canvas.itemconfig(t2, text=f"Register gametype {i}/{len(game_types)}")
            self.canvas.update()

            GameTypes.register(game_type)
            i += 1
        # except Exception as e:
        #     import traceback
        #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
        #     self.scenemanager.change_scene(
        #         "qbubbles:crash_scene",
        #         "".join(list(traceback.format_exception(e.__class__, e)))
        #     )

        # =----- ENTITIES -----= #
        self.canvas.itemconfig(t1, text="Loading Entities")
        self.canvas.itemconfig(t2, text="Initialize entities")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Loading Entities...")

        # try:
        entities = _entitiesInit.init()
        i = 1
        for entity in entities:
            self.canvas.itemconfig(t2, text=f"Register entity {i}/{len(entities)}")
            self.canvas.update()

            # print(entity)

            Entities.register(entity)
            i += 1
        # except Exception as e:
        #     import traceback
        #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
        #     self.scenemanager.change_scene(
        #         "qbubbles:error_scene",
        #         "Initialize Entities failed"
        #         "".join(list(traceback.format_exception_only(e.__class__, e)))
        #     )

        self.canvas.itemconfig(t1, text="Loading Bubbles")
        self.canvas.itemconfig(t2, text="Initialize bubbles")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Loading Bubbles...")

        # try:
        bubble_types = _bubblesInit.init_bubbles()
        for bubble_type in bubble_types:
            self.canvas.itemconfig(t2, text=f"Register bubble {bubble_type.get_key()}")
            Bubbles.register(bubble_type)
        _bubSystem.BubbleSystem.init()
        # except Exception as e:
        #     import traceback
        #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
        #     self.scenemanager.change_scene(
        #         "qbubbles:error_scene",
        #         "Initialize Bubbles failed!",
        #         "".join(list(traceback.format_exception_only(e.__class__, e)))
        #     )

        self.canvas.itemconfig(t2, text="Loading bubble texture configs")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Loading Bubble Texture Configs...")

        bubble_tex_cfg_loader = _res.TextureConfigLoader("bubble")
        # noinspection PyTypeChecker
        for addon in list(Addons.values()) + [None]:
            addon: qbubbles.addon.Addon
            models_bubble = bubble_tex_cfg_loader.load_textureconfigs(addon)

            _gameIO.Logging.info("GameLoader", f"Loading Bubble Texture Configs for {addon.get_addonid() if addon is not None else 'qbubbles'}...")

            # print(bubble_types)
            # print(models_bubble)

            for bubble_type in bubble_types:
                key = bubble_type.get_key().get_key()
                namespace = bubble_type.get_key().get_namespace()
                self.canvas.itemconfig(t2, text=f"Generating bubble image: {namespace}:{key}")
                self.canvas.update()
                _gameIO.Logging.info("GameLoader", f"Generating bubble image: {namespace}:{key}")
                # print(bubble_type.get_key())
                if bubble_type.get_key().__str__() not in models_bubble.keys():
                    warnings.warn(
                        f"Bubble type with key '{bubble_type.get_key()}' have no bubble model", ResourceWarning)

                # images = {}
                images: dict = bubble_tex_cfg_loader.generate_bubble_images(bubble_type.minRadius, bubble_type.maxRadius,
                                                                            models_bubble[bubble_type.get_key()])

                # for radius in range(bubbleObject.minRadius, bubbleObject.maxRadius):
                #     colors = modelsBubble[uname]["Colors"]
                #     images[radius] = utils.createbubble_image((radius, radius), None, *colors)
                for radius, texture in images.items():
                    text = f"Generating bubble image: {namespace}:{key} ({radius})"
                    Textures.register(NamespacedKey.from_string("qbubbles:bubble"), bubble_type.get_key(), texture, radius=radius)
                # Bubbles.register(bubble_type)
            # except Exception as e:
            #     import traceback
            #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
            #     self.scenemanager.change_scene(
            #         "qbubbles:error_scene",
            #         "Loading Bubble Models failed!",
            #         "".join(list(traceback.format_exception_only(e.__class__, e)))
            #     )
                # noinspection PyTypeChecker

        _gameIO.Logging.info("GameLoader", f"Loading Entity Texture Configs...")
        for addon in list(Addons.values()) + [None]:
            addon: qbubbles.addon.Addon
            models_bubble = bubble_tex_cfg_loader.load_textureconfigs(addon)

            _gameIO.Logging.info("GameLoader", f"Loading Entity Texture Configs for {addon.get_addonid() if addon is not None else 'qbubbles'}...")

            entity_tex_cfg_loader = _res.TextureConfigLoader("entity")
            modelsEntity = entity_tex_cfg_loader.load_textureconfigs(addon)
            self.playerModel = modelsEntity[NamespacedKey.from_string("qbubbles:player")]

            for entityName, entityData in modelsEntity.items():
                if entityData["Rotation"]:
                    degrees = entityData['RotationDegrees']
                    self.canvas.itemconfig(t2, text=f"Load images for {entityName} 0 / {int(360 / degrees)}")
                    self.canvas.update()
                    image = _PIL.Image.open(f"assets/textures/entities/{entityData['Image']['Name']}.png")
                    for degree in range(0, 360, entityData["RotationDegrees"]):
                        self.canvas.itemconfig(
                            t2, text=f"Load images for {entityName} {int(degree / degrees)} / {int(360 / degrees)}")
                        self.canvas.update()
                        image_c: _PIL.Image.Image = image.copy()
                        image_c = image_c.rotate(degree, resample=_PIL.Image.BICUBIC)
                        Textures.register(NamespacedKey.from_string("qbubbles:entity"), entityName, _PIL.ImageTk.PhotoImage(image_c), rotation=degree)
                        # print(Registry._registryTextures)
            # except Exception as e:
            #     import traceback
            #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
            #     self.scenemanager.change_scene(
            #         "qbubbles:error_scene",
            #         "Loading Entity Models failed!",
            #         "".join(list(traceback.format_exception_only(e.__class__, e)))
            #     )


        # # TODO: Remove this and use Registry.get_bubresource(...) as above (with .yml files for bubble-models)
        # self.canvas.itemconfig(t2, text="Creating Dicts")
        # self.canvas.update()
        #
        # # Adding the dictionaries for the bubbles. With different res.
        # GAME_CONFIG["BubbleImage"] = dict()
        # GAME_CONFIG["BubbleImage"]["Normal"] = dict()
        # GAME_CONFIG["BubbleImage"]["Triple"] = dict()
        # GAME_CONFIG["BubbleImage"]["Double"] = dict()
        # GAME_CONFIG["BubbleImage"]["Kill"] = dict()
        # GAME_CONFIG["BubbleImage"]["SpeedUp"] = dict()
        # GAME_CONFIG["BubbleImage"]["SpeedDown"] = dict()
        # GAME_CONFIG["BubbleImage"]["Ultimate"] = dict()
        # GAME_CONFIG["BubbleImage"]["Up"] = dict()
        # GAME_CONFIG["BubbleImage"]["Teleporter"] = dict()
        # GAME_CONFIG["BubbleImage"]["SlowMotion"] = dict()
        # GAME_CONFIG["BubbleImage"]["DoubleState"] = dict()
        # GAME_CONFIG["BubbleImage"]["Protect"] = dict()
        # GAME_CONFIG["BubbleImage"]["ShotSpdStat"] = dict()
        # GAME_CONFIG["BubbleImage"]["HyperMode"] = dict()
        # GAME_CONFIG["BubbleImage"]["TimeBreak"] = dict()
        # GAME_CONFIG["BubbleImage"]["Confusion"] = dict()
        # GAME_CONFIG["BubbleImage"]["Paralyse"] = dict()
        # GAME_CONFIG["BubbleImage"]["StoneBub"] = dict()
        # GAME_CONFIG["BubbleImage"]["NoTouch"] = dict()
        # GAME_CONFIG["BubbleImage"]["Key"] = dict()
        # GAME_CONFIG["BubbleImage"]["Diamond"] = dict()
        # GAME_CONFIG["BubbleImage"]["Present"] = dict()
        # GAME_CONFIG["BubbleImage"]["SpecialKey"] = dict()
        #
        # _min = 21
        # _max = 80
        #
        # # Adding the different resolutions to the bubbles.
        # for i in range(_min, _max + 1):
        #     GAME_CONFIG["BubbleImage"]["Normal"][i] = utils.createbubble_image((i, i), None, "white")
        #     GAME_CONFIG["BubbleImage"]["Double"][i] = utils.createbubble_image((i, i), None, "gold")
        #     GAME_CONFIG["BubbleImage"]["Triple"][i] = utils.createbubble_image((i, i), None, "blue", "#007fff", "#00ffff", "white")
        #     GAME_CONFIG["BubbleImage"]["SpeedDown"][i] = utils.createbubble_image((i, i), None, "#ffffff", "#a7a7a7", "#7f7f7f", "#373737")
        #     GAME_CONFIG["BubbleImage"]["SpeedUp"][i] = utils.createbubble_image((i, i), None, "#ffffff", "#7fff7f", "#00ff00", "#007f00")
        #     GAME_CONFIG["BubbleImage"]["Up"][i] = utils.createbubble_image((i, i), None, "#00ff00", "#00ff00", "#00000000", "#00ff00")
        #     GAME_CONFIG["BubbleImage"]["Ultimate"][i] = utils.createbubble_image((i, i), None, "gold", "gold", "orange", "gold")
        #     GAME_CONFIG["BubbleImage"]["Kill"][i] = utils.createbubble_image((i, i), None, "#7f0000", "#7f007f", "#7f0000",)
        #     GAME_CONFIG["BubbleImage"]["Teleporter"][i] = utils.createbubble_image((i, i), None, "#7f7f7f", "#7f7f7f", "#ff1020", "#373737")
        #     GAME_CONFIG["BubbleImage"]["SlowMotion"][i] = utils.createbubble_image((i, i), None, "#ffffffff", "#00000000", "#000000ff")
        #     GAME_CONFIG["BubbleImage"]["DoubleState"][i] = utils.createbubble_image((i, i), None, "gold", "#00000000", "gold", "gold")
        #     GAME_CONFIG["BubbleImage"]["Protect"][i] = utils.createbubble_image((i, i), None, "#00ff00", "#3fff3f", "#7fff7f", "#9fff9f")
        #     GAME_CONFIG["BubbleImage"]["ShotSpdStat"][i] = utils.createbubble_image((i, i), None, "#ff7f00", "#ff7f00", "gold")
        #     GAME_CONFIG["BubbleImage"]["HyperMode"][i] = utils.createbubble_image((i, i), None, "black", "black", "white", "black")
        #     GAME_CONFIG["BubbleImage"]["TimeBreak"][i] = utils.createbubble_image((i, i), None, "red", "orange", "yellow", "white")
        #     GAME_CONFIG["BubbleImage"]["Confusion"][i] = utils.createbubble_image((i, i), None, "black", "purple", "magenta", "white")
        #     GAME_CONFIG["BubbleImage"]["Paralyse"][i] = utils.createbubble_image((i, i), None, "#ffff00", "#ffff00", "#ffff7f", "#ffffff")
        #     GAME_CONFIG["BubbleImage"]["StoneBub"][i] = utils.createbubble_image((i, i), None, "black", "orange", "yellow")
        #     GAME_CONFIG["BubbleImage"]["NoTouch"][i] = utils.createbubble_image((i, i), None, "#7f7f7f", "#7f7f7f", "#7f7f7f", "#373737")
        #
        #     self.canvas.itemconfig(t1, text="Loading Bubbles Sizes")
        #     self.canvas.itemconfig(t2, text="Loading %s of %s" % (i - _min, _max - 1 - _min))
        #     self.canvas.update()
        #
        # # Adding the static-resolution-bubbles.
        # GAME_CONFIG["BubbleImage"]["Key"][60] = PhotoImage(file="assets/bubbles/Key.png")
        # GAME_CONFIG["BubbleImage"]["Diamond"][36] = PhotoImage(
        #     file="assets/bubbles/Diamond.png")
        # GAME_CONFIG["BubbleImage"]["Present"][40] = PhotoImage(
        #     file="assets/bubbles/Present.png")
        # # noinspection PyTypeChecker
        # GAME_CONFIG["BubbleImage"]["Coin"] = PhotoImage(file="assets/CoinBub.png")
        # GAME_CONFIG["BubbleImage"]["SpecialKey"][48] = PhotoImage(
        #     file="assets/bubbles/SpecialMode.png")

        # # TODO: Remove this.
        # for i in GAME_CONFIG["BubbleImage"].keys():
        #     print("%s: %s" % (i, repr(GAME_CONFIG["BubbleImage"][i])))

        # -=================== INTITIALIZE EFFECTS ===================- #
        self.canvas.itemconfig(t1, text="Loading Effects")
        self.canvas.itemconfig(t2, text="Initialize effects")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Loading Effects...")

        baseEffects: _t.List[BaseEffect] = _effectsInit.init_effects()
        for baseEffect in baseEffects:
            self.canvas.itemconfig(t2, text=f"Register effect {baseEffect.get_key()}")
            addon: qbubbles.addon.Addon = Addons.get(baseEffect.get_key().get_namespace())
            Textures.register(NamespacedKey.from_string("qbubbles:effect"), baseEffect.get_key(), _res.Resources.get_asset(Path("assets/textures/icons/effect"), f"{baseEffect.get_key().get_key()}.png", addon=addon))
            Effects.register(baseEffect)

        # -================== INITIALIZE GUI IMAGES ==================- #
        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading GUI Images")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Loading GUI Images...")

        Textures.register_default(
            NamespacedKey.from_string("qbubbles:bubble"), texture=PhotoImage(
                file="assets/textures/defaults/bubble.png"))
        Textures.register_default(
            NamespacedKey.from_string("qbubbles:icons"), texture=PhotoImage(
                file="assets/textures/defaults/icons.png"))

        Textures.register(
            NamespacedKey.from_key("gui"), NamespacedKey.from_key("effect_bar"),
            PhotoImage(file="assets/textures/gui/classic_map/effect_bar.png"), gametype="qbubbles:classic_map")

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Effect Images")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Loading Effect Images...")

        Textures.register_default(NamespacedKey.from_string("qbubbles:effect"), texture=PhotoImage(file="assets/textures/icons/effect/default.png"))

        for item in _os.listdir("assets/textures/icons/effect/"):
            image = PhotoImage(file=f"assets/textures/icons/effect/{item}")
            Textures.register(NamespacedKey.from_key("effect"), NamespacedKey.from_key(f"{_os.path.splitext(item)[0]}"), image, gametype="qbubbles:classic_map")

        # -===================== INITIALIZE ICONS ====================- #
        # Adding ship image...                                          #
        # _reg.Registry.register_image("ShipImage", PhotoImage(file="assets/Ship.png"))

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Icons")
        self.canvas.update()

        _gameIO.Logging.info("GameLoader", f"Loading Icons...")

        # # try:
        #     # Adding Store Item Icons. (SII)
        #     _reg.Registry.register_storeitem("Key", PhotoImage(file="assets/Images/StoreItems/Key.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: Key")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("Teleport", PhotoImage(file="assets/Images/StoreItems/Teleport.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: Teleport")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("Shield", PhotoImage(file="assets/Images/StoreItems/Shield.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: Shield")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("Diamond", PhotoImage(file="assets/Images/StoreItems/DiamondBuy.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: Diamond")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("BuyACake", PhotoImage(file="assets/Images/StoreItems/BuyACake.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: Buy A Cake")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("Pop3Bubbles", PhotoImage(file="assets/Images/StoreItems/Pop_3_bubs.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: Pop 3 Bubbles")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("PlusLife", PhotoImage(file="assets/Images/StoreItems/PlusLife.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: PlusLife")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("Speedboost", PhotoImage(file="assets/Images/StoreItems/SpeedBoost.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: Speedboost")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("SpecialMode", PhotoImage(file="assets/Images/StoreItems/SpecialMode.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Store Item: Special Mode")
        #     self.canvas.update()
        #     _reg.Registry.register_storeitem("DoubleScore", PhotoImage(file="assets/Images/StoreItems/DoubleScore.png"))
        #     self.canvas.itemconfig(t2, text="Loading Icons - Double Score")
        #     self.canvas.update()
        # # except Exception as e:
        # #     import traceback
        # #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
        # #     self.scenemanager.change_scene(
        # #         "qbubbles:error_scene",
        # #         "Loading Icons Stage 1 failed!",
        # #         "".join(list(traceback.format_exception_only(e.__class__, e)))
        # #     )

        # try:
        _gameIO.Logging.info("GameLoader", f"Loading Backgrounds...")

        # Loading backgrounds
        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Background - Line")
        self.canvas.update()
        Textures.register(NamespacedKey.from_key("backgrounds"), NamespacedKey.from_key("line"), PhotoImage(file="assets/LineIcon.png"))

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Background - Normal")
        self.canvas.update()
        Textures.register(NamespacedKey.from_key("backgrounds"), NamespacedKey.from_key("normal"), PhotoImage(file="assets/Images/Backgrounds/GameBG2.png"))
        # _reg.Registry.register_background("Normal", PhotoImage(file="assets/Images/Backgrounds/GameBG2.png"))

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Background - Special Mode")
        self.canvas.update()
        Textures.register(NamespacedKey.from_key("backgrounds"), NamespacedKey.from_key("special"), PhotoImage(file="assets/Images/Backgrounds/GameBG Special2.png"))
        # _reg.Registry.register_background("Special", PhotoImage(file="assets/Images/Backgrounds/GameBG Special2.png"))
        # except Exception as e:
        #     import traceback
        #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
        #     self.scenemanager.change_scene(
        #         "qbubbles:error_scene",
        #         "Loading Background failed",
        #         "".join(list(traceback.format_exception_only(e.__class__, e)))
        #     )

        # try:

        _gameIO.Logging.info("GameLoader", f"Loading Foregrounds...")

        # Loading foregrounds
        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Foreground - For Bubble Gift")
        self.canvas.update()

        Textures.register(NamespacedKey.from_key("foregrounds"), NamespacedKey.from_key("bubble_gift"), PhotoImage(file="assets/EventBackground.png"))
        # _reg.Registry.register_foreground("BubbleGift", PhotoImage(file="assets/EventBackground.png"))

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Foreground - Store FG")
        self.canvas.update()
        Textures.register(NamespacedKey.from_key("foregrounds"), NamespacedKey.from_key("store_fg"), PhotoImage(file="assets/FG2.png"))
        # _reg.Registry.register_foreground("StoreFG", PhotoImage(file="assets/FG2.png"))
        # except Exception as e:
        #     import traceback
        #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
        #     self.scenemanager.change_scene(
        #         "qbubbles:error_scene",
        #         "Loading Foregounds failed",
        #         "".join(list(traceback.format_exception_only(e.__class__, e)))
        #     )

        # try:

        _gameIO.Logging.info("GameLoader", f"Loading Icons...")

        # Loading Icons
        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Icons - Present Circle")
        self.canvas.update()

        Textures.register(NamespacedKey.from_key("icons"), NamespacedKey.from_key("present_circle"), PhotoImage(file="assets/FG2.png"))
        # _reg.Registry.register_icon("PresentCircle", PhotoImage(file="assets/Circle.png"))

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Icons - Present Chest")
        self.canvas.update()
        Textures.register(NamespacedKey.from_key("icons"), NamespacedKey.from_key("present_chest"), PhotoImage(file="assets/FG2.png"))
        # _reg.Registry.register_icon("PresentChest", PhotoImage(file="assets/Present.png"))

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Icons - Store: Diamond & Coin")
        self.canvas.update()
        Textures.register(NamespacedKey.from_key("icons"), NamespacedKey.from_key("store_diamond"), PhotoImage(file="assets/Diamond.png"))
        Textures.register(NamespacedKey.from_key("icons"), NamespacedKey.from_key("store_coin"), PhotoImage(file="assets/Coin.png"))
        # _reg.Registry.register_icon("StoreDiamond", PhotoImage(file="assets/Diamond.png"))
        # _reg.Registry.register_icon("StoreCoin", PhotoImage(file="assets/Coin.png"))

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Icons - Pause")
        self.canvas.update()
        Textures.register(NamespacedKey.from_key("icons"), NamespacedKey.from_key("store_fg"), PhotoImage(file="assets/Pause.png"))
        # _reg.Registry.register_icon("Pause", PhotoImage(file="assets/Pause.png"))

        self.canvas.itemconfig(t1, text="Loading Other Images")
        self.canvas.itemconfig(t2, text="Loading Icons - SlowMotion")
        self.canvas.update()
        Textures.register(NamespacedKey.from_key("foregrounds"), NamespacedKey.from_key("slowmotion_icon"), PhotoImage(file="assets/SlowMotionIcon.png"))
        # _reg.Registry.register_icon("EffectSlowmotion", PhotoImage(file="assets/SlowMotionIcon.png"))
        # except Exception as e:
        #     import traceback
        #     _gameIO.printerr("".join(list(traceback.format_exception_only(e.__class__, e))))
        #     self.scenemanager.change_scene(
        #         "qbubbles:error_scene",
        #         "Loading Icons Stage 2 failed",
        #         "".join(list(traceback.format_exception_only(e.__class__, e)))
        #     )

        # Loading fonts
        GAME_CONFIG["fonts"] = {}

        _gameIO.Logging.info("GameLoader", f"Loading Fonts...")

        self.canvas.itemconfig(t1, text="Loading Fonts")
        self.canvas.itemconfig(t2, text="Title Fonts")
        self.canvas.update()
        GAME_CONFIG["fonts"]["titleButtonFont"] = _utils.Font("Helvetica", 15)

        self.canvas.itemconfig(t1, text="Loading Fonts")
        self.canvas.itemconfig(t2, text="Slots Menu Fonts")
        self.canvas.update()
        GAME_CONFIG["fonts"]["slotsButtonFont"] = _utils.Font("Helvetica", 12)

        _evts.InitializeEvent(self, self.canvas, t1, t2)

        # for bubble in _reg.Registry.get_bubbles():
        #     if _reg.Registry.bubresource_exists(bubble.get_uname()):
        #         continue

        _gameIO.Logging.info("GameLoader", f"Loading Scenes...")

        # Register Scenes
        self.canvas.itemconfig(t1, text="Loading Scenes")
        self.canvas.itemconfig(t2, text="Title Screen")
        Scenes.register(_titleMenu.TitleMenu())
        self.canvas.itemconfig(t2, text="Saves Menu")
        Scenes.register(_scenes.SavesMenu())
        self.canvas.itemconfig(t2, text="Store")
        Scenes.register(_components.Store())
        self.canvas.itemconfig(t2, text="Game")
        Scenes.register(_game.GameScene())

        # Registry.register_mode("teleport", TeleportMode())

        _evts.PostInitializeEvent(self, self.canvas, t1, t2)

        self.canvas.itemconfig(t1, text="DONE!")
        self.canvas.itemconfig(t2, text="")

        self.scenemanager.change_scene("qbubbles:title_screen")
        return

        # # Setting background from nothing to normal.
        # self.back["id"] = self.canvas.create_image(0, 0, anchor="nw", image=self.back["normal"])
        #
        # # Creating shi
        # self.ship["id"] = self.canvas.create_image(7.5, 7.5, image=self.ship["image"])
        # print(self.ship["id"])
        #
        # # Moving ship to position
        # self.canvas.move(self.ship["id"],
        #     self.SAVE_DATA["Entities"]["qbubbles:player"]["objects"][0]["Position"][0],
        #     self.SAVE_DATA["Entities"]["qbubbles:player"]["objects"][0]["Position"][1]
        # )
        #
        # self.canvas.itemconfig(t1, text="Creating Stats objects")
        # self.canvas.itemconfig(t2, text="")
        #
        # # Initializing the panels for the game.
        # self.panels["game/top"] = self.canvas.create_rectangle(
        #     -1, -1, GAME_CONFIG["WindowWidth"], 69, fill="darkcyan"
        # )
