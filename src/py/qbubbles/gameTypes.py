import os
from random import Random
from tkinter import Canvas
from typing import Dict, Tuple, Optional, List

import dill

import qbubbles.addon as addon1
import qbubbles.advUtils.time as time1
import qbubbles.bubbleSystem as bubbleSystem
import qbubbles.bubbles as bubbles1
import qbubbles.config as config
import qbubbles.data as data1
import qbubbles.entities as entities
import qbubbles.events as events
import qbubbles.gameIO as gameIO
import qbubbles.gui as gui
# from qbubbles.nzt import NZTFile
import qbubbles.registries as registries
# from qbubbles.registry import Registry
# from qbubbles.entities import Entity, Player
import qbubbles.util as util
import qbubbles.utils as utils
import qbubbles.gameIO as _gameIO
# import qbubbles.events as events


class GameType(object):
    def __init__(self):
        self.__key: Optional[util.NamespacedKey] = None
        self._bubbles = []
        self._gameobjects = []
        self.player: Optional[entities.Player] = None
        self.seedRandom = None
        self.randoms: Dict[str, Tuple[Random, int]] = {}
        self._uname = None

    def load(self):
        events.MapInitializeEvent.bind(self.on_mapinit)
        events.FirstLoadEvent.bind(self.on_firstload)
        events.LoadCompleteEvent.bind(self.on_loadcomplete)

    def on_loadcomplete(self, evt: 'qbubbles.events.LoadCompleteEvent'):
        pass

    def get_gameobjects(self) -> 'List[qbubbles.entities.Entity]':
        return self._gameobjects

    @staticmethod
    def get_bubbles():
        return data1.SAVE_DATA["Entities"]["qbubbles:bubble"]["objects"]

    def create(self, seed, randoms=None):
        self.seedRandom = seed
        self.randoms: Dict[str, Tuple[Random, int]] = {}
        if randoms is not None:
            for random in randoms:
                randomState = random["State"]
                offset = random["offset"]
                id_ = random["id"]

                # noinspection PyNoneFunctionAssignment,PyTypeChecker
                r: Random = Random(self.seedRandom << offset).setstate(randomState)
                self.randoms[id_] = (r, offset)
        else:
            self.init_defaults()
        self._uname = None

    def add_random(self, id_, offset):
        if id_.count(":") != 1:
            gameIO.printerr(f"Randomizer id must contain a single COLON, id: {id_}")
        self.randoms[id_] = self.format_random(offset)

    def init_defaults(self):
        self.add_random("qbubbles:effect.duration", 24)
        self.add_random("qbubbles:effect.strength", 16)
        self.add_random("qbubbles:bubblesystem", 4096)
        self.add_random("qbubbles:bubble.radius", 32)
        self.add_random("qbubbles:bubble.speed", 64)
        self.add_random("qbubbles:bubble.x", 128)
        self.add_random("qbubbles:bubble.y", 256)

    def on_mapinit(self, evt: 'events.MapInitializeEvent'):
        pass

    def on_firstload(self, evt: 'events.FirstLoadEvent'):
        pass

    def format_random(self, offset):
        if offset % 4 == 0:
            return Random(self.seedRandom << offset), offset
        else:
            raise ValueError("Offset must be multiple of 4")

    def __setattr__(self, key, value):
        if key == "format_random":
            if value != self.format_random:
                raise PermissionError("Cannot set format_random")
        self.__dict__[key] = value

    # def get_uname(self):
    #     return self._uname

    def get_save_data(self):
        randoms = []
        for id, data in self.randoms.items():
            sdata = {}
            random: Random = data[0]
            sdata["State"] = random.getstate()
            sdata["offset"] = data[1]
            sdata["id"] = id
            randoms.append(sdata)

    def create_random_bubble(self, *, x=None, y=None):
        bubbleObject = self.get_random_bubble()
        w = registries.GAME_CONFIG["WindowWidth"]
        h = registries.GAME_CONFIG["WindowHeight"]

        radius = bubbleObject.radius

        if x is None:
            x = self.randoms["qbubbles:bubble.x"][0].randint(0 - radius, w + radius)
        if y is None:
            y = self.randoms["qbubbles:bubble.y"][0].randint(0 - radius, h + radius)
        self.create_bubble(x, y, bubbleObject)

    def get_random_bubble(self) -> 'bubbles1.Bubble':
        bubble: bubbles1.BubbleType = bubbleSystem.BubbleSystem.random(self.randoms["qbubbles:bubblesystem"][0])
        radius = self.randoms["qbubbles:bubble.radius"][0].randint(bubble.minRadius, bubble.maxRadius)
        speed = self.randoms["qbubbles:bubble.speed"][0].randint(bubble.minSpeed, bubble.maxSpeed)

        max_health = 1
        if hasattr(bubble, "maxHealth"):
            max_health = bubble.maxHealth

        return bubbles1.Bubble(
            bubble, max_health, max_health, radius=radius, speed=speed, health=bubble.hardness,
            scoremp=bubble.scoreMultiplier, attackmp=bubble.attackMultiplier, defencemp=bubble.defenceMultiplier)

    def create_bubble(self, x: int, y: int, bubble_object: 'bubbles1.Bubble'):
        assert type(bubble_object) != type

        bubble_object.create(x=x, y=y)
        self._bubbles.append(bubble_object)
        self._gameobjects.append(bubble_object)

    def delete_bubble(self, bubble_object: 'bubbles1.Bubble'):
        self._bubbles.remove(bubble_object)
        self._gameobjects.remove(bubble_object) if bubble_object in self._gameobjects else None
        bubble_object.delete()

    def on_update(self, evt: 'events.UpdateEvent'):
        pass

    # def on_playermotion(self, evt: PlayerMotionEvent):
    #     pass

    def on_collision(self, evt: 'events.CollisionEvent'):
        pass

    def on_keypress(self, evt: 'events.KeyPressEvent'):
        pass

    def on_keyrelease(self, evt: 'events.KeyReleaseEvent'):
        pass

    def on_xinput(self, evt: 'events.XInputEvent'):
        pass

    def get_key(self):
        return self.__key

    def set_uname(self, name):
        self.__key = util.NamespacedKey.from_key(name)

    def __repr__(self):
        return f"<GameType({self.get_key()})>"

    def load_savedata(self, path):
        raise RuntimeError("Default Game Map does not support loading savedata")

    def save_savedata(self, path):
        raise RuntimeError("Default Game Map does not support saving savedata")

    def create_savedata(self, path, seed):
        raise RuntimeError("Default Game Map does not support creating savedata")


class ClassicMap(GameType):
    def __init__(self):
        super(ClassicMap, self).__init__()

        self._pause = False
        self.set_uname("classic_map")
        self.maxBubbles = 50
        self.texts = {}
        self.panelTop: Optional[gui.CPanel] = None
        self.tSpecialColor = "#ffffff"
        self.tNormalColor = "#3fffff"
        self.effectImages = {}
        self.effectX = 100

    def load(self):
        events.MapInitializeEvent.bind(self.on_mapinit)
        events.FirstLoadEvent.bind(self.on_firstload)
        events.LoadCompleteEvent.bind(self.on_loadcomplete)

        # print(self.on_mapinit)

    def init_defaults(self):
        self.add_random("qbubbles:effect.duration", 24)
        self.add_random("qbubbles:effect.strength", 16)
        self.add_random("qbubbles:bubblesystem", 4096)
        self.add_random("qbubbles:bubblesystem.start_x", 8)
        self.add_random("qbubbles:bubblesystem.start_y", 12)
        self.add_random("qbubbles:bubble.radius", 32)
        self.add_random("qbubbles:bubble.speed", 64)
        self.add_random("qbubbles:bubble.x", 128)
        self.add_random("qbubbles:bubble.y", 256)

    def on_effect_apply(self, evt: 'events.EffectApplyEvent'):
        if evt.entity == self.player:
            self.effectX += 256
            translator: util.Translator = registries.TypeRegistry.get_translator()
            key: util.NamespacedKey = evt.appliedEffect.baseObject.get_key()
            addon: addon1.Addon = registries.Addons.get(key.get_namespace())
            lname = translator.get_translation("effect", key.__str__().replace(":", "."), "name", addon=addon)
            name = registries.GAME_CONFIG["language"][lname] if lname in registries.GAME_CONFIG["language"].keys() else lname
            self.effectImages[evt.appliedEffect] = {
                # Textures
                "effectBar": self.canvas.create_image(
                    self.effectX, 5,
                    image=registries.Textures.get(
                        "qbubbles:gui", "qbubbles:effect_bar", gametype=self.get_key().__str__()), anchor="nw"
                ),
                "icon": self.canvas.create_image(
                    self.effectX+1, 6,
                    image=registries.Textures.get(
                        "qbubbles:effect", evt.appliedEffect.get_key(), gametype=self.get_key()), anchor="nw"
                ),

                # Texts
                "text": self.canvas.create_text(
                    self.effectX + 36, 18,
                    text=name % {
                        "strength": int(evt.appliedEffect.strength)
                    }, anchor="w"
                ),
                "time": self.canvas.create_text(
                    self.effectX + 200, 18, text=str(time1.TimeLength(evt.appliedEffect.get_remaining_time())), anchor="w"
                )
            }
            self.canvas.tag_raise(self.effectImages[evt.appliedEffect]["icon"])
        
    def on_firstload(self, evt: 'events.FirstLoadEvent'):
        _gameIO.Logging.info("Gamemap", "Create bubbles because the save is loaded for first time")

        w = registries.GAME_CONFIG["WindowWidth"]
        h = registries.GAME_CONFIG["WindowHeight"]
        for i in range(self.maxBubbles):
            self.create_random_bubble()
        data1.SAVE_DATA["Game"]["GameType"]["initialized"] = True
        self.player.teleport(registries.GAME_CONFIG["MiddleX"], registries.GAME_CONFIG["MiddleY"])

    def on_mapinit(self, evt: 'events.MapInitializeEvent'):
        w = registries.GAME_CONFIG["WindowWidth"]
        h = registries.GAME_CONFIG["WindowHeight"]

        # print("LOL")

        self.seedRandom = data1.SAVE_DATA["Game"]["GameType"]["seed"]
        self.init_defaults()

        canvas: Canvas = evt.canvas

        t1 = evt.t1
        t2 = evt.t2

        # noinspection PyUnusedLocal
        self.panelTop = gui.CPanel(canvas, 0, 0, width="extend", height=69, fill="darkcyan", outline="darkcyan")
        # print(f"Panel Top created: {self.panelTop}")
        panelTopFont = utils.Font("Helvetica", 12)

        # # Initializing the panels for the game.
        # self.panels["game/top"] = canvas.create_rectangle(
        #     -1, -1, GAME_CONFIG["WindowWidth"], 69, fill="darkcyan"
        # )

        # Create seperating lines.
        canvas.create_line(0, 70, registries.GAME_CONFIG["WindowWidth"], 70, fill="lightblue")
        canvas.create_line(0, 69, registries.GAME_CONFIG["WindowWidth"], 69, fill="lightblue")

        translator = registries.TypeRegistry.get_translator()

        canvas.create_text(
            55, 30, text=translator.get_translation("info", "score", addon=None),
            fill=self.tSpecialColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Score")
        canvas.create_text(
            110, 30, text=translator.get_translation("info", "level", addon=None),
            fill=self.tSpecialColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Level")
        canvas.create_text(
            165, 30, text=translator.get_translation("info", "speed", addon=None),
            fill=self.tSpecialColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Speed")
        canvas.create_text(
            220, 30, text=translator.get_translation("info", "lives", addon=None),
            fill=self.tSpecialColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Lives")

        gui.CEffectBarArea(canvas, gametype=self)

        canvas.create_text(1120, 30, text=registries.GAME_CONFIG["language"]["info.tps"],
                           fill=self.tNormalColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Teleports")

        # Coin / Diamond icons
        canvas.create_image(1185, 30, image=registries.Textures.get("qbubbles:icons", "store_diamond"))
        canvas.itemconfig(t2, text="Diamonds")
        canvas.create_image(1185, 50, image=registries.Textures.get("qbubbles:icons", "store_coin"))
        canvas.itemconfig(t2, text="Coins")

        canvas.itemconfig(t1, text="Creating Stats Data")
        canvas.itemconfig(t2, text="")

        # Game information values.
        self.texts["score"] = canvas.create_text(55, 50, fill="cyan")
        canvas.itemconfig(t2, text="Score")
        self.texts["level"] = canvas.create_text(110, 50, fill="cyan")
        canvas.itemconfig(t2, text="Level")
        self.texts["speed"] = canvas.create_text(165, 50, fill="cyan")
        canvas.itemconfig(t2, text="Speed")
        self.texts["lives"] = canvas.create_text(220, 50, fill="cyan")
        canvas.itemconfig(t2, text="Lives")

        self.texts["shiptp"] = canvas.create_text(w-20, 10, fill="cyan")
        canvas.itemconfig(t2, text="Teleports")
        self.texts["diamond"] = canvas.create_text(w-20, 30, fill="cyan")
        canvas.itemconfig(t2, text="Diamonds")
        self.texts["coin"] = canvas.create_text(w-20, 50, fill="cyan")
        canvas.itemconfig(t2, text="Coins")
        self.texts["level-view"] = canvas.create_text(registries.GAME_CONFIG["MiddleX"], registries.GAME_CONFIG["MiddleY"],
                                                      fill='Orange',
                                                      font=utils.Font("Helvetica", 46).get_tuple())
        canvas.itemconfig(t2, text="Level View")

        self.background = gui.CPanel(canvas, 0, 71, "extend", "expand", fill="#00a7a7", outline="#00a7a7")

        self.canvas = canvas

        events.LoadCompleteEvent.bind(self.on_loadcomplete)
        events.EffectApplyEvent.bind(self.on_effect_apply)

        # print(SAVE_DATA["Entities"].keys())

        bubbles = data1.SAVE_DATA["Entities"]["qbubbles:bubble"]["objects"].copy()
        data1.SAVE_DATA["Entities"]["qbubbles:bubble"]["objects"] = []
        for bubble in bubbles:
            bub: bubbles1.BubbleType = registries.Bubbles.get(bubble["ID"])
            pos = bubble["Position"]
            x = pos[0]
            y = pos[1]
            rad = bubble["Attributes"]["radius"]
            spd = bubble["Attributes"]["speed"]
            hlt = bubble["Attributes"]["health"]
            bub_obj = bubbles1.Bubble(bub, bub.hardness)
            bub_obj.reload(bubble)
            self._bubbles.append(bub_obj)
            self._gameobjects.append(bub_obj)
            # self.create_bubble(x, y, bub_obj)

        self.player = entities.Player()
        # print(SAVE_DATA["Entities"].keys())

        if data1.SAVE_DATA["Entities"]["qbubbles:player"]["objects"][0]["Position"]:
            self.player.create(*data1.SAVE_DATA["Entities"]["qbubbles:player"]["objects"][0]["Position"])
            self.player.reload(data1.SAVE_DATA["Entities"]["qbubbles:player"]["objects"][0])
        else:
            self.player.create(registries.GAME_CONFIG["MiddleX"], registries.GAME_CONFIG["MiddleY"])
        self._gameobjects.append(self.player)

        # print(self.player)

    def on_pause(self, evt: 'events.PauseEvent'):
        self._pause = evt.pause

    def on_update(self, evt: 'events.UpdateEvent'):
        if self._pause:
            return

        if len(self._bubbles) < self.maxBubbles:
            bubbleObject = self.get_random_bubble()
            w = registries.GAME_CONFIG["WindowWidth"]
            h = registries.GAME_CONFIG["WindowHeight"]

            x = w + bubbleObject.radius
            y = self.randoms["qbubbles:bubble.y"][0].randint(71 + bubbleObject.radius, h - bubbleObject.radius)
            self.create_bubble(x, y, bubbleObject)
        self.canvas.itemconfig(self.texts["score"], text=f"{self.player.score}")
        self.canvas.itemconfig(self.texts["level"], text=f"{self.player.get_objectdata()['Attributes']['level']}")
        self.canvas.itemconfig(self.texts["lives"], text=f"{round(self.player.health, 1)}")
        self.canvas.itemconfig(self.texts["score"], text=f"{self.player.score}")

        move_left = 0
        for appliedeffect, dict_ in self.effectImages.copy().items():

            self.canvas.itemconfig(dict_["time"],
                                   text=str(time1.TimeLength(appliedeffect.get_remaining_time())))

            self.canvas.move(dict_["icon"], -move_left, 0)
            self.canvas.move(dict_["time"], -move_left, 0)
            self.canvas.move(dict_["text"], -move_left, 0)
            self.canvas.move(dict_["effectBar"], -move_left, 0)

            if appliedeffect.get_remaining_time() < 0:
                appliedeffect.dead = True

            if appliedeffect.dead:
                self.canvas.delete(dict_["icon"])
                self.canvas.delete(dict_["time"])
                self.canvas.delete(dict_["text"])
                self.canvas.delete(dict_["effectBar"])

                del self.effectImages[appliedeffect]
                move_left += 256
                self.effectX -= 256

        # self.texts["score"] = self.player.score
        for bubble in self._bubbles.copy():
            bubble: bubbles1.Bubble
            if not bubble.dead:
                # print((-bubble.radius))
                if bubble.get_coords()[0] < -bubble.baseRadius:
                    bubble.instant_death()
            else:
                self._gameobjects.remove(bubble)
                self._bubbles.remove(bubble)

    def create_random_bubble(self, *, x=None, y=None):
        bubbleObject = self.get_random_bubble()
        w = registries.GAME_CONFIG["WindowWidth"]
        h = registries.GAME_CONFIG["WindowHeight"]

        if x is None:
            x = self.randoms["qbubbles:bubble.x"][0].randint(0 - bubbleObject.radius, w + bubbleObject.radius)
        if y is None:
            y = self.randoms["qbubbles:bubble.y"][0].randint(71 + bubbleObject.radius, h - bubbleObject.radius)
        self.create_bubble(x, y, bubbleObject)

    def on_loadcomplete(self, evt: 'events.LoadCompleteEvent'):
        events.UpdateEvent.bind(self.on_update)
        events.PauseEvent.bind(self.on_pause)
        # CleanUpEvent.bind(self.on_cleanup)
        events.GameExitEvent.bind(self.on_gameexit)
        events.LoadCompleteEvent.unbind(self.on_loadcomplete)
        events.SaveEvent.bind(self.on_save)
        _gameIO.Logging.info("Gamemap", "Load Complete")

        self.player.activate_events()

    def on_gameexit(self, evt: 'events.GameExitEvent'):
        _gameIO.Logging.info("Gamemap", "Exiting Game - Game Map")
        events.UpdateEvent.unbind(self.on_update)
        events.PauseEvent.unbind(self.on_pause)
        # CleanUpEvent.unbind(self.on_cleanup)
        events.GameExitEvent.unbind(self.on_gameexit)
        events.SaveEvent.unbind(self.on_save)

        self._bubbles = []
        self.player.deactivate_events()

    def on_save(self, evt: 'events.SaveEvent'):
        save_path = f"{registries.LAUNCHER_CONFIG['gameDir']}saves/{evt.saveName}"
        return self.save_savedata(save_path)

        # game_data = SAVE_DATA["Game"].copy()
        # game_data["GameType"]["Randoms"] = self.randoms
        # entities_data = SAVE_DATA["Entities"].copy()
        # entity_info_data = SAVE_DATA["EntityInfo"].copy()
        #
        # save_path = f"{LAUNCHER_CONFIG['gameDir']}saves/{evt.saveName}"
        #
        # game_data_file = NZTFile(f"{save_path}/game.dill", "w")
        # game_data_file.data = game_data
        # game_data_file.save()
        # game_data_file.close()
        #
        # entity_info_file = NZTFile(f"{save_path}/entityinfo.dill", "w")
        # entity_info_file.data = entity_info_data
        # entity_info_file.save()
        # entity_info_file.close()
        #
        # os.makedirs(f"{save_path}/entities/")
        #
        # for entity in entities_data.keys():
        #     path = '/'.join(entity.split(":")[:-1])
        #     os.makedirs(f"{save_path}/entities/{path}")
        #     entity_data_file = NZTFile(f"{save_path}/entities/{entity.replace(':', '/')}.dill", "w")
        #     entity_data_file.data = entities_data[entity]
        #     entity_data_file.save()
        #     entity_data_file.close()

    def load_savedata(self, path):
        from pathlib import Path

        # noinspection SpellCheckingInspection
        data1.SAVE_DATA["EntityInfo"] = config.FileGameData(Path(f"{path}/entityinfo.dill")).read()
        data1.SAVE_DATA["Entities"] = {}
        self.maxBubbles = data1.SAVE_DATA["EntityInfo"]["qbubbles:bubble"]["maxAmount"]

        # Get Entity data
        for entity_id in data1.SAVE_DATA["EntityInfo"]["Entities"]:
            entity_path = entity_id.replace(":", "/")
            data = config.FileGameData(Path(f"{path}/entities/{entity_path}.dill")).read()
            data1.SAVE_DATA["Entities"][entity_id] = data

    def save_savedata(self, path):
        save_data = data1.SAVE_DATA.copy()

        # entity_data2 = []

        # Transform Object data into List / Dict data.
        for entity in registries.Entities.values():
            save_data["Entities"][entity.get_key().__str__()]["objects"] = []
            _gameIO.Logging.debug("GameTypeSaving", f"EntityData: {save_data['Entities'][entity.get_key().__str__()]}")
        for entity in self.get_gameobjects():
            save_data["Entities"][entity.get_key().__str__()]["objects"].append(entity.get_objectdata())
        for entity in registries.Entities.values():
            _gameIO.Logging.debug("GameTypeSaving", f"EntityData: {save_data['Entities'][entity.get_key().__str__()]}")
        #  = entity_data2

        game_data = save_data["Game"].copy()
        entity_info_data = save_data["EntityInfo"].copy()
        entity_data = save_data["Entities"].copy()

        with open(f"{path}/game.dill", "wb+") as file:
            dill.dump(game_data, file)
            file.close()
        # game_data_file = NZTFile(f"{path}/game.dill", "w")
        # game_data_file.data = game_data
        # game_data_file.save()
        # game_data_file.close()

        with open(f"{path}/entityinfo.dill", "wb+") as file:
            dill.dump(entity_info_data, file)
            file.close()

        # entity_info_file = NZTFile(f"{path}/entityinfo.dill", "w")
        # entity_info_file.data = entity_info_data
        # entity_info_file.save()
        # entity_info_file.close()

        if not os.path.exists(f"{path}/entities/"):
            os.makedirs(f"{path}/entities/")

        for entity in entity_data.keys():
            entity_path = '/'.join(entity.split(":")[:-1])
            if not os.path.exists(f"{path}/entities/{entity_path}"):
                os.makedirs(f"{path}/entities/{entity_path}", exist_ok=True)

            with open(f"{path}/entities/{entity.replace(':', '/')}.dill", "wb+") as file:
                dill.dump(entity_data[entity], file)
                file.close()

            # entity_data_file = NZTFile(
            #     f"{path}/entities/"
            #     f"{entity.replace(':', '/')}.dill",
            #     "w")
            # entity_data_file.data = entity_data[entity]
            # entity_data_file.save()
            # entity_data_file.close()

    def create_savedata(self, path, seed):
        game_data = {
            "GameInfo": {
                "seed": seed
            },
            "GameType": {
                "id": self.get_key().__str__(),
                "seed": seed,
                "initialized": False,
                "Randoms": []
            }
        }

        def dict_exclude_key(key, d: dict):
            d2 = d.copy()
            del d2[key]
            return d2

        entityinfo_data = {
            "qbubbles:bubble": {
                "speedMultiplier": 5,
                "maxAmount": 100
            },
            "Entities": [
                entities.get_key().__str__() for entities in registries.Entities.values()
            ],
            "EntityData": dict(((s.get_key().__str__(), dict_exclude_key("objects", dict(s.get_entitydata()))) for s in registries.Entities.values()))
        }

        entity_data = dict()
        for entity in registries.Entities.values():
            entity_data[entity.get_key().__str__()] = entity.get_entitydata()

        data1.SAVE_DATA.clear()
        data1.SAVE_DATA.update({"GameData": game_data, "EntityInfo": entityinfo_data, "EntityData": entity_data})

        bubble_data = {"bub-id": [], "bub-special": [], "bub-action": [], "bub-radius": [], "bub-speed": [],
                       "bub-position": [], "bub-index": [], "key-active": False}

        with open(f"{path}/game.dill", "wb+") as file:
            dill.dump(game_data, file)
            file.close()
        # game_data_file = NZTFile(f"{path}/game.dill", "w")
        # game_data_file.data = game_data
        # game_data_file.save()
        # game_data_file.close()

        with open(f"{path}/entityinfo.dill", "wb+") as file:
            dill.dump(entityinfo_data, file)
            file.close()

        # entity_info_file = NZTFile(f"{path}/entityinfo.dill", "w")
        # entity_info_file.data = entity_info_data
        # entity_info_file.save()
        # entity_info_file.close()

        os.makedirs(f"{path}/entities/", exist_ok=True)

        for entity in entity_data.keys():
            entity_path = '/'.join(entity.split(":")[:-1])
            if not os.path.exists(f"{path}/entities/{entity_path}"):
                os.makedirs(f"{path}/entities/{entity_path}", exist_ok=True)

            with open(f"{path}/entities/{entity.replace(':', '/')}.dill", "wb+") as file:
                dill.dump(entity_data[entity], file)
                file.close()

        with open(f"{path}/bubble.dill", "wb+") as file:
            dill.dump(bubble_data, file)

            # game_data_file.data = bubble_data
            # game_data_file.save()
            # game_data_file.close()
