import os
import string
import warnings
from inspect import isclass, getfile
from typing import Optional
from zipimport import zipimporter

from qbubbles import GAME_VERSION
from qbubbles import Registry


warnings.warn("Call to deprecated module", DeprecationWarning)

MODS = []

# noinspection PyPep8Naming,SpellCheckingInspection
# def Addon(*, addonid, name, version, qbversion=GAME_VERSION):
#     if addonid[0] not in string.ascii_letters:
#         raise ValueError(f"Invalid character of modid {repr(addonid)}: '{addonid[0]}' must be a ASCII letter")
#     for character in addonid[1:]:
#         if character not in string.ascii_letters+string.digits:
#             raise ValueError(f"Invalid character of modid {repr(addonid)}: '{character}' must be a ASCII letter")
#     # print(addonid, name, version, qbversion)
#
#     def decorator(func):
#         if not isclass(func):
#             raise TypeError(f"Object '{func.__name___}' is not a class")
#         if qbversion == GAME_VERSION:
#             # MODS.append(dict(addonid=addonid, name=name, version=version, func=func))
#             Registry.register_modobject(modid=addonid, name=name, version=version, func=func)
#             func.addonID = addonid
#             func.name = name
#             func.fpath = getfile(func)
#             func.modPath = os.path.split(func.fpath)[0]
#             func.version = version
#             func.zipimport = None
#     return decorator


class Addon(object):
    def __init__(self, *, addonid, name, version, qbversion=GAME_VERSION):
        if addonid[0] not in string.ascii_letters:
            raise ValueError(f"Invalid character of modid {repr(addonid)}: '{addonid[0]}' must be a ASCII letter")
        for character in addonid[1:]:
            if character not in string.ascii_letters + string.digits + "_":
                raise ValueError(f"Invalid character of modid {repr(addonid)}: '{character}' must be a ASCII letter")

        self.addonid = addonid
        self.name = name
        self.version = version
        self.qbversion = qbversion

    def __call__(self, clazz):
        class AddonDecorator(object):
            def __init__(self):
                # print("__init__ called!")
                # print("args: " + repr([clazz]))
                self.__clazz = clazz
                if not isclass(clazz):
                    raise TypeError(f"Object '{clazz.__name___}' is not a class")
                if parent.qbversion == GAME_VERSION:
                    # MODS.append(dict(addonid=addonid, name=name, version=version, func=func))
                    # Register Addon
                    Registry.register_addon(
                        addonid=parent.addonid, name=parent.name, version=parent.version, clazz=clazz)

                    clazz.addonID = parent.addonid
                    clazz.name = parent.name
                    clazz.fpath = getfile(clazz)
                    clazz.addonPath = os.path.split(clazz.fpath)[0]
                    clazz.version = parent.version
                    clazz.zipimport = None

            def __call__(self, *args, **kwargs):
                self.__clazz(*args, **kwargs)

        parent = self

        return AddonDecorator()


class AddonSkeleton(object):
    addonID: str
    name: str
    version: str
    fpath: str
    modPath: str
    zipimport: Optional[zipimporter]

    def __repr__(self):
        return f"Addon(<{self.modID}>)"


# @Addon(addonid="example", name="Example Addon", version="1.0.0")
# class ExampleAddon(AddonSkeleton):
#     def __init__(self):
#         print(dir(self))
#
#         print(self.addonID)
#
#
# if __name__ == '__main__':
#     ExampleAddon()
