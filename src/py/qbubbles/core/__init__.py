import types
from pathlib import Path, PurePath
from typing import Optional, List
from inspect import FrameInfo


class Trace(object):
    def __init__(self, f_infos: List[FrameInfo] = None):
        # f_infos = []

        # while f_info:
        #     f_infos.append(f_info)
        #     f_info = f_info.f_back

        # f_infos = f_infos

        f_infos.reverse()
        self._f_infos = f_infos

    def find_locals_item(self, item) -> List[FrameInfo]:
        found_frames = []

        for tb in self._f_infos:
            if item in tb.frame.f_locals:
                found_frames.append(tb)

        return found_frames

    def find_file(self, file: Path) -> List[FrameInfo]:
        found_frames = []

        for f_info in self._f_infos:
            if Path(f_info.filename).absolute() == file.absolute():
                found_frames.append(f_info)

        return found_frames

    def find_in_path(self, path: Path) -> List[FrameInfo]:
        found_frames = []

        for f_info in self._f_infos:
            if Path(f_info.filename).absolute().as_posix().startswith(path.absolute().as_posix()):
                pass

        return found_frames

    @classmethod
    def get_current(cls, stack_level=1):
        import inspect

        return cls(inspect.stack()[:-stack_level])


# noinspection PyUnusedFunction
class Radius(object):
    def __init__(self, radius: float):
        self.__radius = radius if isinstance(radius, float) else float(radius)
        
    def get_radius(self) -> float:
        return self.__radius
    
    def set_radius(self, value: float):
        self.__radius = value
        
    def __eq__(self, other):
        if isinstance(other, Radius):
            return self.get_radius() == other.get_radius()
        elif isinstance(other, int):
            return self.get_radius() == other
        elif isinstance(other, float):
            return self.get_radius() == other
        return False
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __gt__(self, other):
        if isinstance(other, Radius):
            return self.get_radius() > other.get_radius()
        elif isinstance(other, int):
            return self.get_radius() > other
        elif isinstance(other, float):
            return self.get_radius() > other
        return False
    
    def __lt__(self, other):
        if isinstance(other, Radius):
            return self.get_radius() < other.get_radius()
        elif isinstance(other, int):
            return self.get_radius() < other
        elif isinstance(other, float):
            return self.get_radius() < other
        return False

    def __ge__(self, other):
        if isinstance(other, Radius):
            return self.get_radius() >= other.get_radius()
        elif isinstance(other, int):
            return self.get_radius() >= other
        elif isinstance(other, float):
            return self.get_radius() >= other
        return False

    def __le__(self, other):
        if isinstance(other, Radius):
            return self.get_radius() <= other.get_radius()
        elif isinstance(other, int):
            return self.get_radius() <= other
        elif isinstance(other, float):
            return self.get_radius() <= other
        return False
    
    def __pow__(self, power, modulo=None):
        if isinstance(power, Radius):
            return self.get_radius() ** power.get_radius()
        elif isinstance(power, int):
            return self.get_radius() ** power
        elif isinstance(power, float):
            return self.get_radius() ** power
        return False

    def __add__(self, value):
        if isinstance(value, Radius):
            return self.get_radius() + value.get_radius()
        elif isinstance(value, int):
            return self.get_radius() + value
        elif isinstance(value, float):
            return self.get_radius() + value
        return False

    def __sub__(self, value):
        if isinstance(value, Radius):
            return self.get_radius() - value.get_radius()
        elif isinstance(value, int):
            return self.get_radius() - value
        elif isinstance(value, float):
            return self.get_radius() - value
        return False

    def __mul__(self, value):
        if isinstance(value, Radius):
            return self.get_radius() * value.get_radius()
        elif isinstance(value, int):
            return self.get_radius() * value
        elif isinstance(value, float):
            return self.get_radius() * value
        return False

    def __truediv__(self, value):
        if isinstance(value, Radius):
            return self.get_radius() / value.get_radius()
        elif isinstance(value, int):
            return self.get_radius() / value
        elif isinstance(value, float):
            return self.get_radius() / value
        return False

    def __mod__(self, value):
        if isinstance(value, Radius):
            return self.get_radius() % value.get_radius()
        elif isinstance(value, int):
            return self.get_radius() % value
        elif isinstance(value, float):
            return self.get_radius() % value
        return False

    def __radd__(self, value):
        if isinstance(value, Radius):
            return value.get_radius() + self.get_radius()
        elif isinstance(value, int):
            return value + self.get_radius()
        elif isinstance(value, float):
            return value + self.get_radius()
        return False

    def __rsub__(self, value):
        if isinstance(value, Radius):
            return value.get_radius() - self.get_radius()
        elif isinstance(value, int):
            return value - self.get_radius()
        elif isinstance(value, float):
            return value - self.get_radius()
        return False

    def __rmul__(self, value):
        if isinstance(value, Radius):
            return value.get_radius() * self.get_radius()
        elif isinstance(value, int):
            return value * self.get_radius()
        elif isinstance(value, float):
            return value * self.get_radius()
        return False

    def __rtruediv__(self, value):
        if isinstance(value, Radius):
            return value.get_radius() / self.get_radius()
        elif isinstance(value, int):
            return value / self.get_radius()
        elif isinstance(value, float):
            return value / self.get_radius()
        return False

    def __rmod__(self, value):
        if isinstance(value, Radius):
            return value.get_radius() % self.get_radius()
        elif isinstance(value, int):
            return value % self.get_radius()
        elif isinstance(value, float):
            return value % self.get_radius()
        return False


if __name__ == '__main__':
    def tests():
        import sys
        import inspect

        def test():
            stack = inspect.stack()
            print(stack)
            print(type(stack))

            assert inspect.istraceback(inspect.stack())

        def test2():
            a = Path("/home/test/lol/main.py").absolute().as_posix()
            b = Path("C:\\home\\test").absolute().as_posix()

            # print(b in {a})

            assert str(a).startswith(str(b))

        # test()
        test2()
    tests()
