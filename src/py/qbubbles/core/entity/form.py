class Form(object):
    def __init__(self, type_: int):
        self.__type = type_

    def __int__(self):
        return self.__type

    def __eq__(self, other: 'Form'):
        if isinstance(other, Form):
            return self.__type == other.get_int()
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_int(self):
        return self.__type

    def __hash__(self):
        return self.__type,


CIRCLE = Form(0)
SQUARE = Form(1)
TRIANGLE = Form(2)
POINT = Form(3)
LINE = Form(4)
RECT = Form(5)
