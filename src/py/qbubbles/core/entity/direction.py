class Direction(object):
    def __init__(self, left: bool, up: bool, down: bool, right: bool):
        self.left: bool = left
        self.up: bool = up
        self.down: bool = down
        self.right: bool = right

    def __eq__(self, other):
        if isinstance(other, Direction):
            return ((self.left == other.left) and (self.right == other.right)) and ((self.up == other.up) and (self.down == other.down))
        return False

    def __ne__(self, other):
        return not (self.__eq__(other))

    def __neg__(self):
        return Direction(not self.left, not self.up, not self.down, not self.right)

    def __hash__(self):
        return self.left, self.up, self.down, self.right


LEFT = Direction(True, False, False, False)
UP = Direction(False, True, False, False)
DOWN = Direction(False, False, True, False)
RIGHT = Direction(False, False, False, True)
