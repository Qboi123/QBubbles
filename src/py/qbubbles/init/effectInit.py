from qbubbles.effects import ScoreMultiplierEffect, SpeedBoostEffect

EFFECTS = []


def init_effects():
    EFFECTS.append(SpeedBoostEffect())
    EFFECTS.append(ScoreMultiplierEffect())

    return EFFECTS
