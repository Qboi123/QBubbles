import typing

import qbubbles.bubbles

BUBBLES = []


# noinspection PyListCreation
def init_bubbles() -> typing.List[qbubbles.bubbles.BubbleType]:
    BUBBLES.append(qbubbles.bubbles.NormalBubble())
    BUBBLES.append(qbubbles.bubbles.DoubleBubble())
    BUBBLES.append(qbubbles.bubbles.TripleBubble())
    BUBBLES.append(qbubbles.bubbles.DoubleStateBubble())
    BUBBLES.append(qbubbles.bubbles.TripleStateBubble())
    BUBBLES.append(qbubbles.bubbles.DamageBubble())
    BUBBLES.append(qbubbles.bubbles.HealBubble())
    return BUBBLES
