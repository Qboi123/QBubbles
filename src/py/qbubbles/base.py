# from qbubbles.registry import Registry

import warnings
warnings.warn("Call to deprecated module, "
              "for axis use the 'core.axis' module, "
              "for directions use the 'core.direction' module", DeprecationWarning)

HORIZONTAL = "horizontal"
VERTICAL = "vertical"

TYPE_DANGEROUS = "dangerous"
TYPE_NEUTRAL = "neutral"

FORM_CIRCLE = "circle"
FORM_RECT = "rectangle/line"
FORM_LINE = "rectangle/line"

# noinspection PyUnusedName
BUB_NOSTATE = "nostate"
# noinspection PyUnusedName
BUB_WITHSTATE = "withstate"

LEFT = "left"
RIGHT = "right"
UP = "up"
DOWN = "down"

TRUE = True
FALSE = False

COLOR_RED = "red"

RED = COLOR_RED

ANY_BUBBLE = "any.bubble"
SHIP = "ship"

OBJ_SHIP = SHIP
OBJ_ANY_BUBBLE = ANY_BUBBLE
OBJ_BARIER = "barier"


# noinspection PyMissingConstructor
class StoreItem:
    def __init__(self, parent):
        """
        Store Item class.

        @param parent:
        """

        self.parent = parent
        self.coins = 0
        self.diamonds = 0

    def __repr__(self):
        return f"StoreItem<{self.__class__.__name__}>"

    def on_buy(self, parent):
        pass

    def on_select(self, parent):
        pass


class Event:
    def __init__(self):
        pass

    def on_update(self, parent):
        pass

    def on_t_update(self, parent):
        pass


DirectionWaring = Warning


ActionIsNoneWarning = Warning
